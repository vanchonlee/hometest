## build
```sh
make go-install
```

## git url
> https://gitlab.com/vanchonlee/hometest.git

## docker image
> vanchon/tiki:6cc3812-development
> https://hub.docker.com/r/vanchon/tiki/tags?page=1&ordering=last_updated

## run

```sh
docker run -p 8080:8080 vanchon/tiki:lastest
```

## test

```sh
curl http://localhost:8080/search?message=asfdssssddd
```