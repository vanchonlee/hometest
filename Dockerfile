FROM golang:1.16.2 AS builder
WORKDIR /go/src/github.com/chonle/compress

COPY . /go/src/github.com/chonle/compress
RUN make go-install

FROM alpine:latest 
COPY --from=builder /go/bin/* /usr/local/sbin/

EXPOSE 8080

COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT ["sh", "/entrypoint.sh"]
CMD ["tiki"]

