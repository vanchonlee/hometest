#!groovy

def helmLint(String chart_dir) {
    // lint helm chart
    sh "helm lint ${chart_dir}"
}

def helmDeploy(Map args) {
    //configure helm client and confirm tiller process is installed
    if (args.dry_run) {
        println "Running dry-run deployment $args.credentialsId"
        withCredentials([file(credentialsId: args.credentialsId, variable: 'KUBECONFIG')]) {
            sh "helm upgrade --dry-run --install ${args.name} ${args.chart_dir} --set image.tag=${args.tag} --namespace ${args.namespace}"
        }
    } else {
        println "Running deployment"
        withCredentials([file(credentialsId: args.credentialsId, variable: 'KUBECONFIG')]) {
            sh("helm upgrade --install --wait ${args.name} --set image.tag=${args.tag} ${args.chart_dir} --namespace ${args.namespace}")
        }

        echo "Application ${args.name} successfully deployed. Use helm status ${args.name} to check"
    }
}

pipeline {
    agent any
   
    environment {
        registry = 'vanchon/tiki'
        registryCredential = 'dockerhub'
        commit = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
        tag = "$commit-$GIT_BRANCH"
        imageName = "$registry:$tag"
        chart_dir = "./hometest"
        name = "hometest"
    }

    stages {
        stage('Env Variables') {
            steps {
                script {
                    String branch = env.GIT_BRANCH;
                    switch(branch) {
                    case "production":
                        env.namespace = "prod"
                        env.ksecret = "kube-pro"
                        break
                    case "staging":
                        env.namespace = "staging"
                        env.ksecret = "kube-stagging"
                        break
                    default:
                        env.namespace = "dev"
                        env.ksecret = "kube-dev"
                        break
                    }
                }
            }
        }

        stage('Build') {
            steps {
                echo 'Compiling and building'
                script {
                    dockerImage = docker.build imageName
                }
            }
        }
        
        stage('Push') {
            steps {
                echo 'Compiling and building'
                script {
                    docker.withRegistry( '', registryCredential ) {
                        dockerImage.push()
                    }
                }
            }
        }

        stage('Remove Unused docker image') {
            steps{
                sh "echo $imageName"
                sh "docker rmi $imageName"
            }
        }

        stage('Testing') {
            steps {
                script {
                    stage("helm lint") {
                        helmLint(chart_dir)
                    }
                    stage("dry_run") {
                        helmDeploy(
                            dry_run: true,
                            name: "$name",
                            tag: "$tag",
                            namespace: env.namespace,
                            chart_dir: "$chart_dir",
                            credentialsId: env.ksecret
                        )
                    }
                }
            }
        }

        stage('Production') {
            when {
                branch 'production'
            }
            steps {
                helmDeploy(
                    name: "$name",
                    tag: "$tag",
                    namespace: env.namespace,
                    chart_dir: "$chart_dir",
                    credentialsId: env.ksecret
                )
            }
        }

        stage('Staging') {
            when {
                branch 'staging'
            }
            steps {
                helmDeploy(
                    name: "$name",
                    tag: "$tag",
                    namespace: env.namespace,
                    chart_dir: "$chart_dir",
                    credentialsId: env.ksecret
                )
            }
        }

        stage('Deployment') {
            when {
                branch 'development'
            }
            steps {
                helmDeploy(
                    name: "$name",
                    tag: "$tag",
                    namespace: env.namespace,
                    chart_dir: "$chart_dir",
                    credentialsId: env.ksecret
                )
            }
        }
    }
}