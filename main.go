package main

import (
	"bytes"
	"fmt"
	"log"
	"strings"

	"github.com/gin-gonic/gin"
)

func compress(message string) string {
	listStr := strings.Split(message, "")
	lenStr := len(listStr)
	var buffer bytes.Buffer

	log.Printf("chars: %s", listStr)

	for i := 0; i < lenStr; i++ {
		count := 1
		for i < lenStr-1 && listStr[i] == listStr[i+1] {
			count++
			i++
		}
		if count == 1 {
			buffer.WriteString(listStr[i])
		} else {
			buffer.WriteString(fmt.Sprintf("%s%d", listStr[i], count))
		}
	}
	return buffer.String()
}

func main() {
	r := gin.Default()
	r.GET("/search", func(c *gin.Context) {
		message := c.Query("message")
		log.Printf("message: %s", message)
		c.JSON(200, gin.H{
			"message": compress(message),
		})
	})
	r.Run(":8080")
}
